package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    int maxItems = 20;
    ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return maxItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() >= maxItems) {
            return false;
        } else {
            items.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i) == item) {
                items.remove(i);
                return;
            }
        }

        throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> remItems = new ArrayList<FridgeItem>();

        // Get expired food
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).hasExpired()) {
                remItems.add(items.get(i));
            }
        }

        // Remove expired
        for (int i = 0; i < remItems.size(); i++) {
            items.remove(remItems.get(i));
        }
        
        return remItems;
    }
}